import java.io.*;
import java.util.*;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {


        LinkedQueue<Point> pointsQueue = new LinkedQueue<>();

        Scanner in = new Scanner(new File("input.txt"));
        PrintWriter out = new PrintWriter(new File("output.txt"));


            while(in.hasNextInt()){
                Point currentPoint = new Point(in.nextInt(), in.nextInt());
                pointsQueue.enqueue(currentPoint);
            }


        in.close();
        out.close();

    }
}
