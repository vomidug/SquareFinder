public class Point {

    private double coordX;
    private double coordY;

    Point(double x, double y){
        this.coordX = x;
        this.coordY = y;
    }

    Point(int x, int y){
        this.coordX = Double.valueOf(x);
        this.coordY = Double.valueOf(y);
    }


    public double getCoordX() {
        return coordX;
    }

    public double getCoordY() {
        return coordY;
    }


}
