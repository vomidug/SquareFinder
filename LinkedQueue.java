import java.util.Currency;
import java.util.Queue;

public class LinkedQueue<Item> {

    int size;
    Node first;
    Node last;


    public class Node {
        public Item data;
        public Node next;
    }


    public LinkedQueue(){

        this.first = null;
        this.last = first;
        size = 0;

    }


    /** Method turns last element into prelast and links it to the new element
     *  which became last
     *  size increments
     * @param somedata is a value we put into a new element of a queue
     */
    public void enqueue(Item somedata) {
        Node oldLast = last;
        last = new Node();
        last.data = somedata;
        if (isEmpty()) first = last;
        else oldLast.next = last;
        size++;
    }


    /** deletes the first element
     *  and makes the second one the first one
     *  decrements the size
     * @return the deleted item
     */
    public Item dequeue() {
        Item item = first.data;
        first = first.next;
        last.next = first;
        size--;
        return item;
    }


    /**
     *
     * @return size of current queue
     */
    public int getSize() {
        return size;
    }


    /**
     *
     * @return true if current queue is empty, otherwise returns false
     */
    public Boolean isEmpty() {
        return (size == 0);
    }


    /**
     *
     * @return the first element, but doesn't delete it,
     * only
     * take a look
     */
    public Item peek() {
        return first.data;
    }


}